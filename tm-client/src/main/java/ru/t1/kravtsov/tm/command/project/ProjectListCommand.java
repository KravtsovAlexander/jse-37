package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.ProjectListRequest;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Show project list.";

    @NotNull
    public static final String NAME = "project-list";

    @Override
    public void execute() {
        System.out.println("[PROJECTS]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortType);
        @NotNull final ProjectListRequest request = new ProjectListRequest(getToken());
        request.setSort(sort);
        @Nullable final List<Project> projects = getProjectEndpoint()
                .listProject(request)
                .getProjects();
        if (projects == null) return;

        int index = 1;
        for (final Project project : projects) {
            @NotNull final String name = project.getName();
            @NotNull final String description = project.getDescription();
            @NotNull final String id = project.getId();
            @NotNull final String status = Status.toName(project.getStatus());
            System.out.printf("%s. (%s) %s : %s : %s\n", index, id, name, description, status);
            index++;
        }
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
