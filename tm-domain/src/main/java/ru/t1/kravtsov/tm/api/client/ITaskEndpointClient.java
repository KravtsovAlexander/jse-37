package ru.t1.kravtsov.tm.api.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.endpoint.IConnectionProvider;
import ru.t1.kravtsov.tm.api.endpoint.ITaskEndpoint;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceSoap() {
        return newInstanceSoap(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceSoap(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpointClient.newInstance(connectionProvider, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceSoap(@NotNull final String host, @NotNull final String port) {
        return IEndpointClient.newInstance(host, port, NAME, SPACE, PART, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceRest() {
        return IEndpointClient.newInstance(HOST, PORT, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceRest(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpointClient.newInstance(connectionProvider, ITaskEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstanceRest(@NotNull final String host, @NotNull final String port) {
        return IEndpointClient.newInstance(host, port, ITaskEndpoint.class);
    }

}
