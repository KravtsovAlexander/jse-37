package ru.t1.kravtsov.tm.api.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.UserLoginRequest;
import ru.t1.kravtsov.tm.dto.request.UserLogoutRequest;
import ru.t1.kravtsov.tm.dto.request.UserViewProfileRequest;
import ru.t1.kravtsov.tm.dto.response.UserLoginResponse;
import ru.t1.kravtsov.tm.dto.response.UserLogoutResponse;
import ru.t1.kravtsov.tm.dto.response.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/AuthEndpoint")
public interface IAuthEndpoint extends IEndpoint {

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @POST
    @NotNull
    @WebMethod
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserLoginResponse login(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/logout")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserLogoutResponse logout(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/profile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    UserViewProfileResponse profile(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    );

}
