package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

}
