package ru.t1.kravtsov.tm.dto.response;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@ApiModel
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    @Nullable
    private String token;

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@Nullable String token) {
        this.token = token;
    }

}
