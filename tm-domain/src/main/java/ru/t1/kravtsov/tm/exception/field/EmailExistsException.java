package ru.t1.kravtsov.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error. Email already exists.");
    }

}
