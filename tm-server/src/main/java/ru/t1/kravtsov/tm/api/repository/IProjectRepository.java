package ru.t1.kravtsov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> removeByName(@NotNull String userId, @NotNull String name);

    void update(@NotNull Project project);

}
