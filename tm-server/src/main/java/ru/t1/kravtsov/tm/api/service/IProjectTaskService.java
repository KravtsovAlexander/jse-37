package ru.t1.kravtsov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    @NotNull
    Task unbindTaskFromProject(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    void removeProjectById(@Nullable String userId, @Nullable String projectId);

    void removeProjects(@Nullable String userId, @NotNull List<Project> projects);

}
