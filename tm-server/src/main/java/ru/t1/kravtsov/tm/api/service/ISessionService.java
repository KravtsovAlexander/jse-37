package ru.t1.kravtsov.tm.api.service;

import ru.t1.kravtsov.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
