package ru.t1.kravtsov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IRepository;
import ru.t1.kravtsov.tm.api.service.IConnectionService;
import ru.t1.kravtsov.tm.api.service.IService;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kravtsov.tm.exception.field.IdEmptyException;
import ru.t1.kravtsov.tm.exception.field.IndexIncorrectException;
import ru.t1.kravtsov.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionSerivce;

    public AbstractService(final @NotNull IConnectionService connectionSerivce) {
        this.connectionSerivce = connectionSerivce;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionSerivce.getConnection();
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        @NotNull M addedModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            addedModel = repository.add(model);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return addedModel;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> add(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull Collection<M> addedModels;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            addedModels = repository.add(models);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return addedModels;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull Collection<M> setModels;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            setModels = repository.set(models);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return setModels;
    }

    @Override
    @SneakyThrows
    public void deleteAll() {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.deleteAll();
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void deleteAll(@NotNull final List<M> models) {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            repository.deleteAll(models);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable M model;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.findOneById(id);
        }
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneByIndex(final @NotNull Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @Nullable M model;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            model = repository.findOneByIndex(index);
        }
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        @Nullable M removedModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            removedModel = repository.remove(model);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return removedModel;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @NotNull final Connection connection = getConnection();
        @Nullable M removedModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            removedModel = repository.removeById(id);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        if (removedModel == null) throw new EntityNotFoundException();
        return removedModel;
    }

    @NotNull
    @Override
    @SneakyThrows
    public M removeByIndex(final @NotNull Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Connection connection = getConnection();
        @Nullable M removedModel;
        try {
            @NotNull final IRepository<M> repository = getRepository(connection);
            removedModel = repository.removeByIndex(index);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        if (removedModel == null) throw new EntityNotFoundException();
        return removedModel;
    }

    @Override
    @SneakyThrows
    public boolean existsById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.existsById(id);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

}
