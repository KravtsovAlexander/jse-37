package ru.t1.kravtsov.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.service.IConnectionService;
import ru.t1.kravtsov.tm.api.service.IPropertyService;

import java.sql.Connection;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final BasicDataSource dataSource;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.dataSource = dataSource();
    }

    private BasicDataSource dataSource() {
        @NotNull final BasicDataSource result = new BasicDataSource();
        result.setUrl(propertyService.getDbUrl());
        result.setUsername(propertyService.getDbUser());
        result.setPassword(propertyService.getDbPassword());
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(false);
        return connection;
    }

}
