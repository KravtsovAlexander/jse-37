package ru.t1.kravtsov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.service.IConnectionService;
import ru.t1.kravtsov.tm.api.service.IProjectService;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final @NotNull IConnectionService connectionSerivce) {
        super(connectionSerivce);
    }

    @Override
    protected @NotNull IProjectRepository getRepository(final @NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project project = new Project(name);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.add(userId, project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project project = new Project(name, description);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            project = repository.add(userId, project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description != null ? description : "");
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description != null ? description : "");
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull List<Project> projects;
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            projects = repository.removeByName(userId, name);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project changeProjectStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IProjectRepository repository = getRepository(connection);
            repository.update(project);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

}
