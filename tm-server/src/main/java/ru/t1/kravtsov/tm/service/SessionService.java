package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ISessionRepository;
import ru.t1.kravtsov.tm.api.service.IConnectionService;
import ru.t1.kravtsov.tm.api.service.ISessionService;
import ru.t1.kravtsov.tm.model.Session;
import ru.t1.kravtsov.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(final @NotNull IConnectionService connectionSerivce) {
        super(connectionSerivce);
    }

    @NotNull
    @Override
    protected ISessionRepository getRepository(final @NotNull Connection connection) {
        return new SessionRepository(connection);
    }

}
