package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.kravtsov.tm.api.repository.IProjectRepository;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.*;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.exception.entity.UserNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.marker.UnitCategory;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.repository.ProjectRepository;
import ru.t1.kravtsov.tm.repository.TaskRepository;
import ru.t1.kravtsov.tm.repository.UserRepository;
import ru.t1.kravtsov.tm.util.HashUtil;

import java.sql.Connection;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Ignore
@Category(UnitCategory.class)
public class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    final Connection connection = connectionService.getConnection();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository(connection);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(connection);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IUserService service = new UserService(connectionService, taskService, projectService, propertyService);

    @NotNull
    private final User alphaUser = new User();

    @NotNull
    private final User betaUser = new User();

    @NotNull
    private final User gammaUser = new User();

    @Before
    public void before() {
        alphaUser.setId("alpha-user-id");
        alphaUser.setLogin("alpha");
        alphaUser.setEmail("alpha@user.com");
        betaUser.setId("beta-user-id");
        betaUser.setRole(Role.ADMIN);
        betaUser.setLogin("beta");
        betaUser.setEmail("beta@user.com");
        gammaUser.setId("gamma-user-id");
        gammaUser.setLogin("gamma");
        gammaUser.setEmail("gamma@user.com");

        userRepository.add(alphaUser);
        userRepository.add(betaUser);
        userRepository.add(gammaUser);
    }

    @After
    public void after() {
        userRepository.clear();
    }

    @Test
    public void create() {
        @NotNull final User user = service.create("new", "qwerty");
        Assert.assertEquals("new", user.getLogin());
        Assert.assertSame(user, service.findByLogin("new"));

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, "qwerty");
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", "qwerty");
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty");
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty");
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", "");
        });
    }

    @Test
    public void createWithEmail() {
        @NotNull final User user = service.create("new", "qwerty", "new@user.com");
        Assert.assertEquals("new", user.getLogin());
        Assert.assertEquals("new@user.com", user.getEmail());
        Assert.assertSame(user, service.findByLogin("new"));

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, "qwerty", "new@user.com");
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", "qwerty", "new@user.com");
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty", "new@user.com");
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty", "new@user.com");
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", null, "new@user.com");
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", "", "new@user.com");
        });
        Assert.assertThrows(EmailExistsException.class, () -> {
            service.create("test", "qwerty", "new@user.com");
        });
    }

    @Test
    public void createWithRole() {
        @NotNull final User user = service.create("new", "qwerty", Role.ADMIN);
        Assert.assertEquals("new", user.getLogin());
        Assert.assertEquals(Role.ADMIN, user.getRole());
        Assert.assertSame(user, service.findByLogin("new"));

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create(null, "qwerty", Role.ADMIN);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.create("", "qwerty", Role.ADMIN);
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty", Role.ADMIN);
        });
        Assert.assertThrows(LoginExistsException.class, () -> {
            service.create("new", "qwerty", Role.ADMIN);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", null, Role.ADMIN);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.create("test", "", Role.ADMIN);
        });
        Assert.assertThrows(RoleEmptyException.class, () -> {
            final Role role = null;
            service.create("test", "qwerty", role);
        });
    }

    @Test
    public void removeByLogin() {
        @NotNull final User user = service.removeByLogin(alphaUser.getLogin());
        Assert.assertSame(alphaUser, user);
        Assert.assertNull(service.findByLogin(alphaUser.getLogin()));

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.removeByLogin("");
        });
    }

    @Test
    public void removeByEmail() {
        @NotNull final User user = service.removeByEmail(alphaUser.getEmail());
        Assert.assertSame(alphaUser, user);
        Assert.assertNull(service.findByLogin(alphaUser.getLogin()));

        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.removeByEmail("");
        });
    }

    @Test
    public void remove() {
        @Nullable final User removedUser = service.remove(alphaUser);
        Assert.assertSame(alphaUser, removedUser);
        Assert.assertFalse(service.existsById(alphaUser.getId()));

        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.remove(null);
        });
    }

    @Test
    public void setPassword() {
        service.setPassword(alphaUser.getId(), "123");
        Assert.assertEquals(HashUtil.salt(propertyService, "123"), alphaUser.getPasswordHash());

        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword(null, "123");
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.setPassword("", "123");
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(alphaUser.getId(), null);
        });
        Assert.assertThrows(PasswordEmptyException.class, () -> {
            service.setPassword(alphaUser.getId(), "");
        });
    }

    @Test
    public void updateUser() {
        @NotNull final String newFirstName = "NewFirstName";
        @NotNull final String newLastName = "NewLastName";
        @NotNull final String newMiddleName = "NewMiddleName";

        @NotNull final User updatedUser = service.updateUser(alphaUser.getId(), newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(newFirstName, alphaUser.getFirstName());
        Assert.assertEquals(newLastName, alphaUser.getLastName());
        Assert.assertEquals(newMiddleName, alphaUser.getMiddleName());
        Assert.assertSame(alphaUser, service.findByLogin(updatedUser.getLogin()));

        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser(null, newFirstName, newLastName, newMiddleName);
        });
        Assert.assertThrows(IdEmptyException.class, () -> {
            service.updateUser("", newFirstName, newLastName, newMiddleName);
        });

    }

    @Test
    public void lockUserByLogin() {
        alphaUser.setLocked(false);
        service.lockUserByLogin(alphaUser.getLogin());
        Assert.assertTrue(alphaUser.isLocked());

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.lockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.lockUserByLogin("not-existing-user");
        });
    }

    @Test
    public void unlockUserByLogin() {
        alphaUser.setLocked(true);
        service.unlockUserByLogin(alphaUser.getLogin());
        Assert.assertFalse(alphaUser.isLocked());

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.unlockUserByLogin("");
        });
        Assert.assertThrows(UserNotFoundException.class, () -> {
            service.unlockUserByLogin("not-existing-user");
        });
    }

    @Test
    public void findByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.findByLogin("");
        });
    }

    @Test
    public void findByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.findByEmail("");
        });
    }

    @Test
    public void doesLoginExist() {
        Assert.assertTrue(service.doesLoginExist(alphaUser.getLogin()));
        Assert.assertFalse(service.doesLoginExist("not-existing-user-login"));

        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.doesLoginExist(null);
        });
        Assert.assertThrows(LoginEmptyException.class, () -> {
            service.doesLoginExist("");
        });
    }

    @Test
    public void doesEmailExist() {
        Assert.assertTrue(service.doesEmailExist(alphaUser.getEmail()));
        Assert.assertFalse(service.doesEmailExist("not-existing-user-email"));

        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.doesEmailExist(null);
        });
        Assert.assertThrows(EmailEmptyException.class, () -> {
            service.doesEmailExist("");
        });
    }

    @Test
    public void findAll() {
        @NotNull final List<User> users = service.findAll();
        Assert.assertEquals(Arrays.asList(alphaUser, betaUser, gammaUser), users);
    }

    @Test
    public void findAllWithComparator() {
        Comparator<User> comparator = (o1, o2) -> {
            if (o1 == null || o2 == null) return 0;
            return o2.getLogin().compareTo(o1.getLogin());
        };
        Assert.assertEquals(Arrays.asList(gammaUser, betaUser, alphaUser), service.findAll(comparator));

        comparator = null;
        Assert.assertEquals(Arrays.asList(alphaUser, betaUser, gammaUser), service.findAll(comparator));
    }

    @Test
    public void add() {
        @NotNull final User user = new User();
        user.setLogin("test");
        @NotNull final User addedUser = service.add(user);
        Assert.assertSame(user, addedUser);
        Assert.assertSame(user, service.findByLogin(user.getLogin()));
        Assert.assertFalse(user.getLogin().isEmpty());
    }

    @Test
    public void addCollection() {
        @NotNull List<User> users = Collections.emptyList();
        Assert.assertEquals(Collections.emptyList(), service.add(users));
        Assert.assertEquals(Arrays.asList(alphaUser, betaUser, gammaUser), service.findAll());
    }

    @Test
    public void set() {
        @NotNull List<User> users = Collections.emptyList();
        Assert.assertEquals(Collections.emptyList(), service.set(users));
        Assert.assertEquals(Arrays.asList(alphaUser, betaUser, gammaUser), service.findAll());
    }

}
